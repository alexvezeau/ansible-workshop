# Lab 2
## Objectif 

Créez votre fichier inventaire (hosts.txt) dans le répertoire playbooks (il doit y avoir trois groupes : windows, web et app). Vous pouvez utiliser VI ou encore Visual Studio Code.  
À l’aide de la commande ad-hoc Ansible , effectuez les tâches suivantes sur cet inventaire

- Testez la connexion Ansible à tous vos hôtes à l’aide du module Ping
- Installez le paquetage nano seulement sur vos hôtes du groupe Web 
- Changez SELINUX en mode permissif sur tous les hôtes
